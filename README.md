## Overview

このサンプルはMaximilianさんのUdemyの講座に出たサンプルをTypeScriptで再作成したサンプルです。
Eslintでエラーが出ないようにいくつかの修正を行いました。

## How to set up the app

.env.localファイルを用意してFirebaseのURLを記入する。

```bash
touch .env.local
vim .env.local

FIREBASE_EVENTS_DATA_URL = "[firebaseのurl]/events.json"
```

packageをインストールする。

```bash
yarn install
```

yarnがインストールされていない場合は、先にyarnをインストールする。

```bash
npm install -g yarn
```

devサーバーを起動する。

```bash
yarn dev
```

ブラウザで確認する。

http://localhost:3000

![NextEvents.gif](./public/NextEvents.gif)
