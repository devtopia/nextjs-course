import React, { ReactNode } from "react";

import MainHeader from "./main-header";

interface ILayout {
  children: ReactNode;
}

const Layout: React.FC<ILayout> = (props) => {
  return (
    <>
      <MainHeader />
      <main>{props.children}</main>
    </>
  );
};

export default Layout;
