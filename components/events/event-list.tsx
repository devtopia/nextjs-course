import React from "react";

import { IEvent } from "../../types";
import EventItem from "./event-item";
import classes from "./event-list.module.css";

export type EventType = Omit<IEvent, "description" | "isFeatured">;
export type EventListType = {
  items: EventType[];
};

const EventList: React.FC<EventListType> = (props) => {
  const { items } = props;
  return (
    <ul className={classes.list}>
      {items.map((event) => (
        <EventItem
          key={event.id}
          id={event.id}
          image={event.image}
          title={event.title}
          date={event.date}
          location={event.location}
        />
      ))}
    </ul>
  );
};

export default EventList;
