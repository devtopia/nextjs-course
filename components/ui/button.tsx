import Link from "next/link";
import React, { ReactNode } from "react";

import classes from "./button.module.css";

interface IButton {
  link: string;
  children: ReactNode;
  onClick?: () => void;
}

const Button: React.FC<IButton> = (props) => {
  if (props.link) {
    return (
      <Link href={props.link} className={classes.btn}>
        {props.children}
      </Link>
    );
  }

  return (
    <button className={classes.btn} onClick={props.onClick}>
      {props.children}
    </button>
  );
};

export default Button;
