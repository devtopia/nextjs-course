import React, { ReactNode } from "react";

import classes from "./logistics-item.module.css";

interface ILogisticsItem {
  icon: React.ElementType;
  children: ReactNode;
}
const LogisticsItem: React.FC<ILogisticsItem> = (props) => {
  const { icon: Icon } = props;

  return (
    <li className={classes.item}>
      <span className={classes.icon}>
        <Icon />
      </span>
      <span className={classes.content}>{props.children}</span>
    </li>
  );
};

export default LogisticsItem;
