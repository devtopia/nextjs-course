import React, { ReactNode } from "react";

import classes from "./event-content.module.css";

interface IEventContent {
  children: ReactNode;
}

const EventContent: React.FC<IEventContent> = (props) => {
  return <section className={classes.content}>{props.children}</section>;
};

export default EventContent;
