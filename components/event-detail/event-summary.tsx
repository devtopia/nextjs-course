import React from "react";

import classes from "./event-summary.module.css";

interface IEventSummary {
  title: string;
}

const EventSummary: React.FC<IEventSummary> = (props) => {
  const { title } = props;

  return (
    <section className={classes.summary}>
      <h1>{title}</h1>
    </section>
  );
};

export default EventSummary;
