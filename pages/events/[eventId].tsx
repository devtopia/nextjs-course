import { GetStaticPaths, GetStaticProps } from "next";
import Head from "next/head";
// import { useRouter } from "next/router";
import React from "react";

import EventContent from "../../components/event-detail/event-content";
import EventLogistics from "../../components/event-detail/event-logistics";
import EventSummary from "../../components/event-detail/event-summary";
import Button from "../../components/ui/button";
import ErrorAlert from "../../components/ui/error-alert";
import { getAllEvents, getEventById } from "../../data/dummy-data";
import { getFeaturedEvents } from "../../helpers/api-util";
import { IEvent } from "../../types";

interface IEventDetail {
  selectedEvent: IEvent;
}

const EventDetailPage: React.FC<IEventDetail> = (props) => {
  // const router = useRouter();
  // console.log(router.pathname);
  // console.log(router.query);
  // const eventId = router.query.eventId as string;
  // const event = getEventById(eventId);
  const event = props.selectedEvent;

  if (!event) {
    return (
      <>
        <ErrorAlert>
          <p>No event found!</p>
        </ErrorAlert>
        <div className="center">
          <Button link="/events">Show All Events</Button>
        </div>
      </>
    );
  }

  return (
    <>
      <Head>
        <title>{event.title}</title>
        <meta name="description" content={event.description} />
      </Head>
      <EventSummary title={event.title} />
      <EventLogistics
        date={event.date}
        address={event.location}
        image={event.image}
        imageAlt={event.title}
      />
      <EventContent>
        <p>{event.description}</p>
      </EventContent>
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const eventId = context?.params?.eventId as string;
  const event = await getEventById(eventId);
  return {
    props: { selectedEvent: event },
    revalidate: 10,
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  // const events = await getAllEvents();
  const events = await getFeaturedEvents();
  const paths = events.map((event) => ({ params: { eventId: event.id } }));
  return {
    paths: paths,
    fallback: "blocking",
  };
};

export default EventDetailPage;
