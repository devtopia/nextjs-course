// import { GetServerSideProps } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
// import { ParsedUrlQuery } from "querystring";
import React, { useEffect, useState } from "react";
import useSWR from "swr";

import EventList from "../../components/events/event-list";
import ResultsTitle from "../../components/events/results-title";
import Button from "../../components/ui/button";
import ErrorAlert from "../../components/ui/error-alert";
// import { getFilteredEvents } from "../../helpers/api-util";
import { IEvent } from "../../types";

// interface IFilteredEvents {
//   hasError?: boolean;
//   items: IEvent[];
//   date: {
//     year: number;
//     month: number;
//   };
// }

// interface Params extends ParsedUrlQuery {
//   slug: string[];
// }

// const FilteredEventsPage: React.FC<IFilteredEvents> = (props) => {
const FilteredEventsPage = () => {
  const [loadedEvents, setLoadedEvents] = useState<IEvent[]>();
  const router = useRouter();
  const filterData = router.query.slug as string[];
  const { data, error } = useSWR(
    "https://nextjs-course-5e6c5-default-rtdb.firebaseio.com/events.json",
    (url) => fetch(url).then((res) => res.json())
  );

  useEffect(() => {
    if (data) {
      const events = [];

      for (const key in data) {
        events.push({
          id: key,
          ...data[key],
        });
      }
      setLoadedEvents(events);
    }
  }, [data]);

  let pageHeadData = (
    <Head>
      <title>Filtered Events</title>
      <meta name="description" content={`A list of filtered events.`} />
    </Head>
  );

  if (!loadedEvents) {
    return (
      <>
        {pageHeadData}
        <p className="center">Loading...</p>;
      </>
    );
  }

  const filteredYear = filterData[0];
  const filteredMonth = filterData[1];
  const numYear = +filteredYear;
  const numMonth = +filteredMonth;

  pageHeadData = (
    <Head>
      <title>Filtered Events</title>
      <meta
        name="description"
        content={`All events for ${numYear}/${numMonth}.`}
      />
    </Head>
  );

  const filteredEvents = loadedEvents.filter((event) => {
    const eventDate = new Date(event.date);
    return (
      eventDate.getFullYear() === numYear &&
      eventDate.getMonth() === numMonth - 1
    );
  });

  if (
    isNaN(numYear) ||
    isNaN(numMonth) ||
    numYear > 2030 ||
    numYear < 2021 ||
    numMonth < 1 ||
    numMonth > 12 ||
    error
  ) {
    <>
      {pageHeadData}
      <ErrorAlert>
        <p>Invalid filter. Please adjust your values!</p>
      </ErrorAlert>
      <div className="center">
        <Button link="/events">Show All Events</Button>
      </div>
    </>;
  }

  // const filteredEvents = props.items;

  if (!filteredEvents || filteredEvents.length === 0) {
    return (
      <>
        {pageHeadData}
        <ErrorAlert>
          <p>No events found for the chosen filter!</p>
        </ErrorAlert>
        <div className="center">
          <Button link="/events">Show All Events</Button>
        </div>
      </>
    );
  }

  const date = new Date(numYear, numMonth - 1);

  return (
    <>
      {pageHeadData}
      <ResultsTitle date={date} />
      <EventList items={filteredEvents} />
    </>
  );
};

/*
export const getServerSideProps: GetServerSideProps = async (context) => {
  // https://stackoverflow.com/questions/69742129/property-slug-does-not-exist-on-type-parsedurlquery-undefined
  const { slug } = context.params as Params;
  const filterData = slug;
  const filteredYear = filterData[0];
  const filteredMonth = filterData[1];

  const numYear = +filteredYear;
  const numMonth = +filteredMonth;

  if (
    isNaN(numYear) ||
    isNaN(numMonth) ||
    numYear > 2030 ||
    numYear < 2021 ||
    numMonth < 1 ||
    numMonth > 12
  ) {
    return {
      props: { hasError: true },
      // notFound: true,
      // redirect: {
      //   destination: "/error"
      // }
    };
  }

  const filteredEvents = await getFilteredEvents({
    year: numYear,
    month: numMonth,
  });

  return {
    props: {
      items: filteredEvents,
      date: {
        year: numYear,
        month: numMonth,
      },
    },
  };
};
 */
export default FilteredEventsPage;
