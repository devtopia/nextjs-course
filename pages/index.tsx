import { GetStaticProps } from "next";
import Head from "next/head";
import React from "react";

import EventList, { EventListType } from "../components/events/event-list";
import { getFeaturedEvents } from "../helpers/api-util";

const HomePage: React.FC<EventListType> = (props) => {
  return (
    <div>
      <Head>
        <title>NextJS Events</title>
        <meta
          name="description"
          content="Find a lot of great events that allow you to evolve..."
        />
      </Head>
      <h1>The Home Page</h1>
      <EventList items={props.items} />
    </div>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const featuredEvents = await getFeaturedEvents();
  return {
    props: { items: featuredEvents },
    revalidate: 10,
  };
};
export default HomePage;
